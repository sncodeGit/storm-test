# storm-test

Задание из университета

## Сервисы

* nimbus - контейнер storm с запущенным внутри демоном nimbus, в него подключается storm.yam, с некоторыми переопределенными параметрами. Это необходимо поскольку иначе возникают ошибки запуска JVM при загрузке топологии.
* supervisor - контейнер storm с запущенным внутри supervisor-ом
* zookeeper

## Топология

Для примера реализована ExclamationTopology:
* spout (word) - генерирует случайные слова
* bolt (exclaim1) - принимает слова из word и добавляет в конец `!!!`
* bolt (exclaim2) - принимает слова из exclaim1 и добавляет в конец `!!!`

Таким образом, в итоге должны быть слова с `!!!!!!` в конце.

## Сборка кода топологии

В `topology/` представлен пример топологии (ExclamationTopology)
* Зависимости в pom.xml были устаревшими (в центральном репозитории maven их уже нет). Можно решить добавлением неофициальных репозиториев, в данном случае: `repository.jboss.org-public`
```
  <repositories>
    <repository>
      <id>repository.jboss.org-public</id>
      <name>JBoss.org Maven repository</name>
      <url>https://repository.jboss.org/nexus/content/groups/public</url>
    </repository>  
  </repositories>
```
* Собирается с помощью Docker на openjdk-11
```
docker build .
# Запускаем контейнер
docker run <IMAGE_ID>
# Копируем jar из контейнера
docker cp <CONTAINER_ID>:/app/target ./target
```

## Запуск

```
docker-compose up -d
# После запуска можно запустить UI
# Получить к нему доступ можно на localhost:8080
docker exec -it <NIMBUS_ID> storm ui
# Далее нужно запустить топологию на nimbus
# Для этого скопируем собранный jar-файл внутрь контейнера
docker cp topology/jar_save/ExclamationTopology.jar <NIMBUS_ID>:/ExclamationTopology.jar
# Далее запустим эту топологию
docker exec -it <NIMBUS_ID> storm jar /ExclamationTopology.jar org.apache.storm.starter.ExclamationTopology
# Теперь топология должна отображаться в UI
# Также результат работы можно посмотреть в логах в контейнере supervisor
docker exec -it <SUPERVISOR_ID> cat /logs/workers-artifacts/<PATH_TO_LOG_FILE>
```
